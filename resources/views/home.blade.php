@extends('layouts.master')


@section('title')
    Max Heckel - Columbus Ohio Web Engineer
@stop


@section('body')
    <div class="home">
        <div class="hero">
            <div class="container">

                <div class="col-md-1 col-md-offset-1">
                    <img class="photo" src="/images/photo.jpg">
                </div>
                <div class="col-md-9 details">
                    <h1>MAX HECKEL</h1>
                    <h2>Senior Web Developer</h2>
                    <p>A <b>highly</b> motivated, <b>passionate</b> web <b>geek</b>. </p>
                </div>

            </div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                <polygon class="svg--sm" fill="#3FA7EB" points="0,0 30,100 65,21 90,100 100,75 100,100 0,100"/>
                <polygon class="svg--lg" fill="white" points="0,0 15,100 33,21 45,100 50,75 55,100 72,20 85,100 95,50 100,80 100,100 0,100" />
            </svg>
        </div>


        <div class="about">
            <div class="container">
                <div class="col-md-12">
                    <h2>About Me</h2>
                    <div class="row">
                        <div class="col-md-4">
                            <h3>My Work</h3>
                            I've been a full stack web developer professionally for more than half a decade now but I've been coding since I was 10.  From starting my own company to building the infrastructure for multi million dollar e-commerce sites I've worked in a ton of environments.
                        </div>
                        <div class="col-md-4">
                            <h3>My Tech</h3>
                            The languages I'm most comfortable with are PHP, Golang, Javascript, HTML and CSS.  I've used these technologies in production: Laravel, Kubernetes, Vue.js, React, SASS, AWS, Postgres, MySQL, Typescript, Angular, and Docker.  I've also dabbled in python, processing, objective-c and numerous other languages and technologies.
                        </div>
                        <div class="col-md-4">
                            <h3>My Ethos</h3>
                            An avid hiker, biker and gamer I love experiencing life to it's fullest. Coding is my passion and I expect to continue working and learning for a long time.
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="technologies">
            <div class="container">
                <h2>Tech I Use Often</h2>
                <div class="tech">
                    <a target="_blank" href="http://php.net/"> <img style="margin-top: 36px;" src="/images/php.svg"></a>
                </div>

                <div class="tech">
                    <a target="_blank" href="https://golang.org"> <img src="images/go.jpeg"></a>
                </div>
                <div class="tech">
                    <a target="_blank" href="http://laravel.com"> <img src="/images/laravel.png"></a>
                </div>
                <div class="tech">
                    <a target="_blank" href="https://www.javascript.com/"> <img src="/images/js.jpg"></a>
                </div>
                <div class="tech">
                    <a target="_blank" href="https://www.postgresql.org/"> <img src="/images/postgres.png"></a>
                </div>
                <div class="tech">
                    <a target="_blank" href="https://vuejs.org/"> <img src="/images/vue.png"></a>
                </div>
                <div class="tech">
                    <a target="_blank" href="https://aws.amazon.com/"> <img style="margin-top: 23px;" src="/images/aws.png"></a>
                </div>
                <div class="tech">
                    <a target="_blank" href="https://www.linux.com/what-is-linux"> <img src="images/linux.png"></a>
                </div>
            </div>
        </div>

        <div class="projects">
            <div class="container">
                <h2>Projects</h2>
                <div class="row print-syndicate project">
                    <div class="col-md-2">
                        <a target="_blank" href="https://mheducation.com"><img src="/images/mhe-logo.png"></a>
                    </div>
                    <div class="col-md-10">
                        <h3><a target="_blank" href="http://printsyndicate.com"> McGraw Hill Education</a></h3>
                        <p>
                            Currently managing 3 teams comprised of 14 engineers total. Created copy and sharing APIs using Golang to perform extremely efficient copies of courseware and other MHE content. Built and architected a new assignments service for tracking student assignments with numerous integrations between external and internal services. Used Angular 7/9 to create rich SPAs for hundreds of thousands of instructors and students.
                            <br><br>
                            Tech Used: Golang, Angular 7/9, Postgres, PHP, AWS (EC2, ECS, Elasticache, S3, CloudSearch), CircleCI, Docker
                        </p>
                    </div>
                </div>
                <div class="row print-syndicate project">
                    <div class="col-md-2">
                        <a target="_blank" href="https://www.lookhuman.com"> <img src="/images/human-logo.svg"></a>
                        <a target="_blank" href="https://www.mericamade.com"> <img class="half-width" src="/images/merica-logo.svg"></a>
                        <a target="_blank" href="https://www.activateapparel.com"> <img class="half-width activate" src="/images/activate-logo.svg"></a>

                    </div>
                    <div class="col-md-10">
                        <h3><a target="_blank" href="http://printsyndicate.com"> Print Syndicate</a></h3>
                        <p>Completely re-built and re-architected a custom built ecommerce platform.  Built using Laravel, Symfony and Vue.js it servers tens of thousands of unique users a day.  Set up an advanced infrastructure on AWS using ECS clusters and multiple EC2 instances.  Built an extremely comprehensive backroom to allow for reporting, modification and monitoring of the multiple brands hosted on the platform.</p>
                    </div>
                </div>
                <div class="row patronart project">
                    <div class="col-md-2">
                        <a target="_blank" href="https://patronart.com"> <img src="/images/patronart.png"></a>
                    </div>
                    <div class="col-md-10">
                        <h3><a target="_blank" href="http://patronart.com"> PatronArt</a></h3>
                        <p>
                            Built a custom ecommerce/marketplace allowing users to buy art and commission work from artists around the world.  Implemented escrow services for the artists so funds can be stored prior to the artist complete their commissions, insuring they get paid.  Implemented a complex build process, minimizing scripts and page load time.
                        </p>
                    </div>
                </div>
                <div class="row patronart project">
                    <div class="col-md-2">
                        <img src="/images/crowdentials.png">
                    </div>
                    <div class="col-md-10">
                        <h3> Crowdentials</h3>
                        <p>
                            A company I co-founded out of college with 2 partners.  Raised just under half a million dollars in funding.  Built an entire secure platform for investors to be verified by the companies they are investing in for 506(c) offerings.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Contact Info</h3>
                        <b>Phone</b> 937.469.0519<br>
                        <b>Email</b> heckel.max@gmail.com<br>
                        <b>Location</b> Columbus, Ohio
                    </div>
                    <div class="col-md-4 resume">
                        <h3>Download My Resume</h3>
                        <a href="/Max_Heckel_Resume.pdf">
                            <i class="fa fa-download" aria-hidden="true"></i>

                            <h4 style="padding-left: 18px">Resume.pdf</h4>
                        </a>
                    </div>
                    <div class="col-md-4 contact">
                        <style>
                            .special{
                                opacity: 0;
                                position: absolute;
                                top: 0;
                                left: 0;
                                height: 0;
                                width: 0;
                                z-index: -1;
                            }
                        </style>
                        <h3>Send Me A Message</h3>
                        <form method="post" action="/contact">
                            {{csrf_field()}}
                            <input class="notspecial" type="text" name="name212341" placeholder="Name" required>
                            <input class="notspecial" type="email" name="email212341" placeholder="Email" required>
                            <input class="special" autocomplete="off" type="text" id="name" name="name" placeholder="Your name here">
                            <input class="special" autocomplete="off" type="email" id="email" name="email" placeholder="Your e-mail here">

                            <textarea placeholder="Message" name="message" required></textarea>
                            <button type="submit">Send Message</button>
                        </form>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    @if(Session::has('success'))
        <script>
            swal('Success!', 'Your message has been sent, thanks!', 'success');
        </script>
    @endif


@stop

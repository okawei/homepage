@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($posts as $post)
            <a href="/dashboard/post/{{$post->id}}">
                <h1>{{$post->title}}</h1>
            </a>
        @endforeach
    </div>
@stop


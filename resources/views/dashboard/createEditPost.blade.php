@extends('layouts.app')

@section('head')
    <!-- Include external CSS. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">


    <!-- Include Editor style. -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.0/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.0/css/froala_style.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.0/css/plugins/code_view.min.css' rel='stylesheet' type='text/css' />

@stop

@section('content')
    <div class="container new-post">
        <form method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <label>
                Title
                <input name="title" class="form-control title" placeholder="title" v-model="title">
            </label>

            <label>
                Slug:<br>
                <span class="slug" v-text="'https://maxheckel.me/blog/'+slug"></span>
                <input type="hidden" name="slug" v-model="slug">
            </label>

            <label>
                Header Image
                <input type="file" name="headerImage">
                Or<br>
                <input class="form-control" type="text" name="headerImageURL" placeholder="url">

            </label>
            @if($post->id)
                <div class="headerImage" style="background-image: url({{$post->headerImage}})"></div>
            @endif


            <label>Content</label>
            <textarea name="content">{{$post->content}}</textarea>
            <br>
            <label>
                Keywords
                <input type="text" class="form-control" name="keywords" value="{{$keywords}}">
            </label>
            <button class="btn btn-primary">
                Submit
            </button>
            @if($post->id)
                <a class="btn btn-danger pull-right" onclick="javascript:confirmDelete()">Delete post</a>
            @endif
        </form>

    </div>

    <br><br><br>


@stop

@section('scripts')


    <!-- Include external JS libs. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

    <!-- Include JS file. -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.0/js/froala_editor.min.js'></script>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.0/js/plugins/code_view.min.js'></script>
    <script>
        $(function() { $('textarea').froalaEditor({
            toolbarButtons: ['undo', 'redo' , '|', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'outdent', 'indent', 'clearFormatting', 'insertTable', 'html'],
            toolbarButtonsXS: ['undo', 'redo' , '-', 'bold', 'italic', 'underline'],
            htmlRemoveTags: []
        }) });

    </script>
    <script>
        const post = new Vue({
            el: '.new-post',
            data: {
                title: '{{$post->title}}'
            },
            computed: {
                slug: function(){
                    String.prototype.replaceAll = function(search, replacement) {
                        var target = this;
                        return target.split(search).join(replacement);
                    };

                    return encodeURIComponent(this.title.toLowerCase().replaceAll(' ', '-'));
                }
            }
        })

        function confirmDelete(){
            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function(){
                        window.location = '/dashboard/post/{{$post->id}}/delete'
                    });
        }
    </script>


@stop
@extends('blog.layout')

@section('head')
    @parent
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.9.0/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.9.0/highlight.min.js"></script>
@stop

@section('content')

    <div class="post full">
        <div class="hero" style="background-image: url({{$post->headerImage}});">
            <a class="back-to-blog" href="/blog"><span class="lnr lnr-chevron-left"></span> View All Posts</a>
            <h1>
                {{$post->title}}
            </h1>
        </div>
        <div class="content">

            <div class="posted-on">
                Posted On Posted on: {{date('Y-m-d', strtotime($post->created_at))}} at {{date('h:ma', strtotime($post->created_at))}}
            </div>
            {!! $post->content !!}


        </div>
        <div class="disqus">
            <div id="disqus_thread"></div>
            <script>

                /**
                 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                /*
                 var disqus_config = function () {
                 this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                 this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                 };
                 */
                (function() { // DON'T EDIT BELOW THIS LINE
                    var d = document, s = d.createElement('script');
                    s.src = 'https://homepage-2.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

        </div>
    </div>

@stop
@extends('layouts.master')

@section('head')
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
@stop
@section('body')
    <div class="blog index">
        <div class="content">

           @yield('content')

        </div>
        <div class="sidebar">
            <h1><span class="lnr lnr-rocket"></span> Max Heckel</h1>
            <h2>Web & Software Development</h2>
            <br><br>
            I build websites and apps using the latest tech on the web.  I've also been known to dabble in the internet of things and design.
            <div class="social">
                <a href="https://twitter.com/maxheckel" target="_blank"> <i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="https://maxheckel.me" target="_blank"> <i class="fa fa-home" aria-hidden="true"></i></a>
                <a href="mailto:heckel.max@gmail.com"> <i class="fa fa-envelope" aria-hidden="true"></i></a>
                <a target="popup" href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{Request::url()}}','name','width=600,height=400')"> <i class="fa fa-facebook" aria-hidden="true"></i></a>
            </div>

            <div class="topics">
                <h3>Popular Topics</h3>
                <ul>
                    @foreach($topics as $topic)
                        <li><a href="/blog/topic/{{$topic->keyword}}"> {{$topic->keyword}}</a></li>
                    @endforeach
                </ul>

            </div>
        </div>
    </div>

@stop
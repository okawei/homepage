@extends('blog.layout')

@section('content')
    <div class="topic">
        <a class="back-to-blog" href="/blog"><span class="lnr lnr-chevron-left"></span> View All Posts</a>
        <h1><span class="lnr lnr-arrow-right"></span> {{ucwords($topic)}} Posts <span class="lnr lnr-arrow-left"></span></h1>
        <div class="posts">

            @foreach($posts as $post)
                <div class="post preview" >

                    <div class="hero" style="background-image: url({{$post->headerImage}});">
                        <h2>{{$post->title}}</h2>
                    </div>
                    <sub>Posted on: {{date('Y-m-d', strtotime($post->created_at))}} at {{date('h:ma', strtotime($post->created_at))}}</sub>
                    <div class="post-content">
                        {!! $post->preview() !!}
                    </div>
                    <a href="/blog/{{$post->slug}}" class="read-more"><span class="lnr lnr-chevron-right"></span> Read More</a>
                </div>
            @endforeach
            <center>
                {{$posts->render()}}
            </center>
        </div>
    </div>


@stop
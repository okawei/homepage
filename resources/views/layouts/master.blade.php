<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="/css/app.css">
    <script src="https://use.fontawesome.com/75113943ad.js"></script>
    <link rel="stylesheet" href="/sweetalert/dist/sweetalert.css">
    <script src="/sweetalert/dist/sweetalert.min.js"></script>
    @yield('head')
</head>
<body>
    @yield('body')
</body>
</html>

<?php

use Illuminate\Database\Seeder;

class SeedPosts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $images = ['https://static.pexels.com/photos/25318/pexels-photo-25318.jpg', 'https://static.pexels.com/photos/261184/pexels-photo-261184.jpeg', 'https://static.pexels.com/photos/238116/pexels-photo-238116.jpeg', 'https://static.pexels.com/photos/322454/pexels-photo-322454.jpeg', 'https://static.pexels.com/photos/198166/pexels-photo-198166.jpeg', 'https://static.pexels.com/photos/129833/pexels-photo-129833.jpeg'];
        foreach(range(0, 15) as $x){

            /** @var \App\Post $post */
            $post = \App\Post::create([
                'title'=>$faker->sentence,
                'content'=>implode(' ', $faker->paragraphs),
                'slug'=>strtolower(str_replace(' ', '-', $faker->sentence)),
                'headerImage'=>$faker->randomElement($images)
            ]);

            foreach(range(0, 10) as $y){
                $keyword = \App\Keyword::firstOrCreate([
                    'keyword'=>$faker->word
                ]);
                $post->keywords()->attach($keyword->id);
            }
        }
    }
}

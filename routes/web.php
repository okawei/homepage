<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Route::auth();
Route::group(['prefix'=>'/blog'], function(){
    Route::get('/', 'BlogController@getHome');
    Route::get('/{slug}', 'BlogController@getPost');
    Route::get('/topic/{tag}', 'BlogController@getTopic');
});

Route::post('/contact', 'ContactController@postSendMessage');

Route::group(['prefix'=>'/dashboard', 'middleware'=>'auth'], function(){
    Route::get('/', 'DashboardController@getIndex');
    Route::get('post/create', 'DashboardController@getCreatePost');
    Route::post('post/create', 'DashboardController@postCreatePost');
    Route::get('post/{id}', 'DashboardController@getEditPost');
    Route::post('post/{id}', 'DashboardController@postEditPost');
    Route::get('post/{id}/delete', 'DashboardController@getDeletePost');
});

Route::get('/resume', 'HomeController@resume');

Route::get('chat', 'ChatController@getChat');




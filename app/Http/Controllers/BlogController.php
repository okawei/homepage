<?php

namespace App\Http\Controllers;

use App\Keyword;
use App\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function __construct()
    {
        $topics = \DB::select(\DB::raw('SELECT keywords.*, count(keyword_post.keyword_id) as count from keywords, keyword_post where keywords.id = keyword_post.keyword_id group by keywords.id order by count desc limit 10'));
        \View::share('topics', $topics);
    }

    public function getHome(){
        $posts = Post::orderBy('created_at', 'desc')->paginate(5);
        return view('blog.home', compact('posts'));
    }

    public function getPost($slug){
        $post = Post::where('slug', $slug)->first();
        return view('blog.post', compact('post'));
    }

    public function getTopic($topic){
        $posts = Post::whereHas('keywords', function($q) use ($topic){
            $q->where('keyword', $topic);
        })->orderBy('created_at', 'desc')->paginate(5);
        return view('blog.topic', compact('posts', 'topic'));

    }
}


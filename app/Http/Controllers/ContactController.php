<?php

namespace App\Http\Controllers;

use App\Mail\SendMessage;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function postSendMessage(Request $request)
    {
        if($request->get('name') != null || $request->get('email') != null){
            return;
        }
        \Mail::to('heckel.max@gmail.com')->send(new SendMessage(
            $request->get('name212341'),
            $request->get('email212341'),
            $request->get('message')
        ));
        return redirect()->back()->with('success', true);
    }
}

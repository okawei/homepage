<?php

namespace App\Http\Controllers;

use App\Post;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    private $postRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->middleware('auth');
        $this->postRepository = $postRepository;
    }

    public function getIndex(){
        $posts = Post::orderBy('created_at', 'desc')->paginate(10);
        return view('dashboard.index', compact('posts'));
    }

    public function getCreatePost(){
        return view('dashboard.createEditPost', ['post'=>new Post(), 'keywords'=>'']);
    }

    public function postCreatePost(Request $request){
        $postCheck = Post::where('slug', $request->slug)->first();
        if($postCheck != null)
            return redirect()->back()->with('error', 'Slug already exists');
        $post = $this->postRepository->createOrUpdatePost(new Post(), $request);

        return redirect('/dashboard/post/'.$post->id)->with('success', 'Successfully created post!');
    }

    public function getEditPost($id){
        $post = Post::find($id);

        $keywords = array_reduce($post->keywords->toArray(), function($carry, $keyword){
            $carry.=$keyword['keyword'].', ';
            return $carry;
        });
        $keywords = trim($keywords, ', ');
        return view('dashboard.createEditPost', ['post'=>$post, 'keywords'=>$keywords]);
    }

    public function postEditPost(Request $request, $id){
        $post = Post::find($id);
        $this->postRepository->createOrUpdatePost($post, $request);
        return redirect()->back()->with('success', 'Successfully updated post!');
    }

    public function getDeletePost($id){
        $post = Post::find($id);
        $post->delete();
        return redirect('/dashboard')->with('success', 'Successfully deleted post');
    }
}

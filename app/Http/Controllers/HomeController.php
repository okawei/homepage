<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function resume()
    {
        return [
            'name' => 'Max Heckel',
            'title' => 'Lead Software Engineer',
            'profile' => 'Passionate and knowledgeable lead/senior software engineer with over 10 years professional experience. Built and managed software at companies ranging from startups to Fortune 500 companies.',
            'contact' => [
                'phone' => '9374690519',
                'email' => 'heckel.max@gmail.com',
                'location' => [
                    'city' => 'Columbus',
                    'state' => 'Ohio'
                ]
            ],
            'links' => [
                'https://maxheckel.me',
                'https://github.com/maxheckel'
            ],
            'skills' => [
                'languages' => 'Golang, Javascript/Typescript, PHP, Node, SQL, HTML, CSS',
                'frameworks' => 'Laravel, Symfony, Angular, Vue, Gorm',
                'infrastructure' => 'EC2, RDS, ECS, Elasticache, S3, Cloud Search, Kubernetes',
                'misc_tech' => 'Docker, CircleCI, Jenkins, Protractor',
                'misc' => 'Bouldering, Top Rope Climbing, GMing, Mandolin'
            ],
            'employment_history' => [
                [
                    'title' => 'Lead software engineer',
                    'company' => 'McGraw Hill Education',
                    'dates' => [
                        'start' => 'February 2017',
                        'end' => null
                    ],
                    'description' => 'Currently managing 3 teams comprised of 14 engineers total. Created copy and sharing APIs using Golang to perform extremely efficient copies of courseware and other MHE content. Built and architected a new assignments service for tracking student assignments with numerous integrations between external and internal services. Used Angular 7/9 to create rich SPAs for hundreds of thousands of instructors and students.',
                    'tech' => ['Golang', 'Angular 7/9',
                        'Postgres',
                        'PHP',
                        'EC2',
                        'ECS',
                        'Elasticache',
                        'S3',
                        'CloudSearch',
                        'CircleCI',
                        'Docker'
                    ],
                ],
                [
                    'title' => 'Senior software engineer',
                    'company' => 'Print Syndicate',
                    'dates' => [
                        'start' => 'May 2015',
                        'end' => 'February 2017'
                    ],
                    'description' => 'Completely re-built and re-architected a custom built ecommerce platform. Built using Laravel, Symfony and Vue.js it servers tens of thousands of unique users a day. Set up an advanced infrastructure on AWS using ECS clusters and multiple EC2 instances. Built an extremely comprehensive backroom to allow for reporting, modification and monitoring of the multiple brands hosted on the platform. After leaving the position in 2016 I continued to do contracting work for the company.',
                    'tech' => [
                        'Laravel',
                        'PHP',
                        'Symfony',
                        'Vue.js',
                        'Postgres',
                        'AWS',
                        'Docker'
                    ],
                ],
                [
                    'title' => 'CTO/Co-founder',
                    'company' => 'Crowdentials',
                    'dates' => [
                        'start' => 'January 2012',
                        'end' => 'May 2015'
                    ],
                    'description' => 'A company I co-founded out of college with 2 partners. Raised just under half a million dollars in funding. Built an entire secure platform to verify investors income by the companies they are investing in via 506(c) offerings.',
                    'tech' => ['PHP', 'SQL', 'AWS'
                    ],
                ],
            ],
            'education' => [
                [
                    'field' => 'Computer Science',
                    'school' => 'Ohio University',
                    'dates' => [
                        'start' => 'August 2008',
                        'end' => 'January 2013'
                    ]
                ]
            ]
        ];
    }
}

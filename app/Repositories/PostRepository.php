<?php
namespace App\Repositories;

use App\Keyword;
use App\Post;

class PostRepository{
    public function createOrUpdatePost(Post $post, $request){
        \DB::beginTransaction();
        {
            $keywords = explode(',', $request->keywords);


            $file = $request->file('headerImage');
            $name = $post->headerImage;
            if($file != null)
                $name = \Storage::disk('uploads')->put('', $file);
            if($request->headerImageURL != null){
                $name = $request->headerImageURL;
            }
            $post->title = $request->title;
            $post->slug = $request->slug;
            $post->headerImage = $name;
            $post->content = $request->content;
            $post->save();
            $keywords = array_map(function($keyword) use ($post){
                $keyword = trim($keyword);
                $keyword = Keyword::firstOrCreate([
                    'keyword'=>$keyword
                ]);

                return $keyword->id;
            }, $keywords);
            $post->keywords()->sync($keywords);

        }
        \DB::commit();
        return $post;
    }
}
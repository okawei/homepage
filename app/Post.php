<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    public $fillable = [
        'title',
        'content',
        'slug',
        'headerImage'
    ];

    public function keywords(){
        return $this->belongsToMany(Keyword::class);
    }

    public function preview($lines = 4){
        $content = explode('. ', $this->content);
        $content = array_slice($content, 0, $lines, true);
        return implode('. ', $content).'...';
    }

    public function getHeaderImageAttribute($image){
        if(strpos($image, 'http') !== false || strpos($image, 'https') !== false){
            return $image;
        }
        return \URL::to('/uploads/'.$image);
    }


}
